# Example of MOXA <-> FC_Server COM-port connection

## quick start 

1. download&install **Virtual Port Driver 9.0 by Eltima Software** [this link](https://www.eltima.com/ru/products/vspdxp/)
2. create classic COM port pair. COM1-COM2
3. ``cd <project root folder>``
4. ``npm i``
5. ``npm run start`` 
6. observer results. 

If you see something like this, all well.
```
   server listen port:  COM2
   client connected to port: COM1
   req is:  <Buffer 02 05 00 01 ff 00 dd c9>
   { err: 'crcMismatch',
     message: 'the response payload does not match the crc' }
   req is:  <Buffer 11 01 00 00 00 0d ff 5f>
```

## run jsmodbus tests

1. ``cd <project root folder>``
2. ``npm i``
3. ``cd node_modules/jsmodbus`` don`t forget win/unix path style
4. ``npm i mocha && npm i sinon``
5. ``mocha test/*`` for unix, OR ``mocha test\*`` for windows
