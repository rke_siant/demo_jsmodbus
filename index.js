const config = require('config');
const client = require('./modules/modbus/RTU/client');
const server = require('./modules/modbus/RTU/server');

const fc = new server.ServerRTU(
    config.get('server.PORT'),
    config.get('OPTIONS'),
    {
        coils: Buffer.alloc(config.get('server.COILS_BUFFER_SIZE')),
        holding: Buffer.alloc(config.get('server.HOLDING_REGS_BUFFER_SIZE')),
        input: Buffer.alloc(config.get('server.INPUT_REGS_BUFFER_SIZE')),
        discrete: Buffer.alloc(config.get('server.COILS_BUFFER_SIZE')),
    }
);

const moxa = new client.ClientRTU(
    config.get('client.PORT'),
    config.get('OPTIONS'),
    config.get('client.ADDRESS')
    );


moxa.testReq();
