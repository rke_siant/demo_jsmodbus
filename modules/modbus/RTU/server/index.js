'use strict';

// FC_SERVER
const Modbus = require('jsmodbus');
const SerialPort = require('serialport');

class ServerRTU {

    constructor(port, socket_options, server_options) {
        this.socket = new SerialPort(port, socket_options);
        this.server = new Modbus.server.RTU(this.socket, server_options);

        this.socket.on('data', function (req) {
            console.log('req is: ', req);

        });

        console.log('server listen port: ', port);
    }

}

module.exports.ServerRTU = ServerRTU;
