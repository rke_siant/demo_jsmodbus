'use strict';
// MOXA
const Modbus = require('jsmodbus');
const SerialPort = require('serialport');

class ClientRTU {

    constructor(port, socket_options, address){
        this.port = new SerialPort(port, socket_options);
        this.client = new Modbus.client.RTU(this.port, address);

        this.port.on('open', () => {
            console.log(`client connected to port: ${port}`);
            this.client.readCoils(0,13).then( (resp) => {
                console.log('request from server: ', resp);
            }, console.error);
        });
    }

    testReq() {
        const request = Buffer.from([
            0x02, // address
            0x05, // function code
            0x00, 0x01, // address
            0xFF, 0x00, // value
            0xDD, 0xC9  // CRC
        ]);
        this.port.write(request)
    }

}

module.exports.ClientRTU = ClientRTU;


